//the server for famwork to create our API
import express from "express" 

import cors from 'cors'

//arm for mongodb
// modéliser la structure de données,créer des modèles qui vous permettent d'interagir avec les collections MongoDB 
import mongoose from 'mongoose'

import {userRouter} from './routes/users.js'
import {recipesRouter} from './routes/recipes.js'

//initializing an Express application
const app=express()

//ajouter middleware permet de parser requêtes HTTP en JSON
app.use(express.json())
// activer le middleware CORS
app.use(cors())

app.use("/auth" ,userRouter)
app.use("/recipes" ,recipesRouter)

//genrrate a connection
mongoose.connect("mongodb://127.0.0.1:27017/recipess")


app.listen(3001 , () => console.log("SERVER STARTED"))

